#!/bin/bash
# This script polls and logs the spindown state of disks to `$1/tmp/hdd_spindown.status`. Useful to verify that HDD spindown tools like hd-idle work as intended.
#
# Parameters:
#
# - `$1`: directory to store (temporary) cache and log files; will default to the current working directory if unspecified
#
# Requirements:
#
# - Unix tools (OpenWrt package names):
#   - `lsblk`
#   - `smartmontools`
#
# - Bash scripts:
#   - [`cache_device_file_assignment.sh`](https://gitlab.com/salim_b/bash/hdd_spindown/-/raw/master/cache_device_file_assignment.sh)
#   - [`read_in_drives.sh`](https://gitlab.com/salim_b/bash/hdd_spindown/-/raw/master/read_in_drives.sh)
#
# - Config files:
#   - `drives.toml` with a line for each of the desired HDDs consisting of the partition label as key and the device type (as smartctl expects it) as value
#
# Notes:
#
# The first run of the script might take considerably longer because all of the specified drives are spun up by `lsblk` during partition label detection.

# determine directory from which the script is run (necessary for portability)
WD="${BASH_SOURCE%/*}"
if [[ ! -d "$WD" ]]; then WD="$PWD"; fi

# determine directory to temporarily store cache and log files
if [[ -z "$1" ]] ; then
  temp_dir="$WD"
else
  temp_dir="${1%/}"
fi

# read in `drives.toml` to `$drives`
source "${WD}/read_in_drives.sh"

# cache device file assignment to `$lsblk_file`
source "${WD}/cache_device_file_assignment.sh" "${temp_dir}"

log_file="${temp_dir}/hdd_spindown_log.md"
lsblk_file="${temp_dir}/lsblk_name-label-model.txt"
datetime=$(date +"%Y-%m-%dT%T%z")

for label in ${!drives[*]} ; do

    device=$(grep "$label" "${lsblk_file}" | grep -o "sd.")

    if [ -n "${device}" ] ; then

        # get spindown state
        # (explicitly set the device type in order to skip smartctl's auto-detection which may result in spinning up the drive)
        spindown_state=$(smartctl -d "${drives[$label]}" -n standby "/dev/${device}" | grep -Eo "Device is in .+ mode" | grep -Eo "([A-Z_]{2,}.*)*[A-Z_]{2,}")

        echo "${datetime} ${label} /dev/${device} ${spindown_state}" >> "${log_file}"
    else
        echo "WARNING: No device file found for partition label \`${label}\`"
    fi
done
