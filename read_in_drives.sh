#!/bin/bash
# This script reads in the [TOML](https://github.com/toml-lang/toml#readme) config file `drives.toml` specifying partition labels (keys) and their device types (values).
#
# The device type of a drive can be obtained by running `smartctl --device=test /dev/sdX` (in most cases it will be `sat` which stands for _**S**CSI to **A**TA **T**ranslation_; more information about device types can be found in the [smartctl manual](https://man.cx/smartctl#heading4)).

# determine directory from which the script is run (necessary for portability)
WD="${BASH_SOURCE%/*}"
if [[ ! -d "$WD" ]]; then WD="$PWD"; fi

# read in hash table of partition labels and device types as smartctl expects it as value:
unset drives
declare -A drives

function fill_drives {

    while read -r line || [[ -n $line ]]; do
        [[ "$line" =~ ^#|^! ]] && continue
        if [[ "${line% =*}" ]]; then drives[${line% =*}]="${line#*= }" ; fi
    done < "$1"
}

if [ ! -f "$WD/drives.toml" ] ; then
    echo "Config file \`$WD/drives.toml\` not found!"
    exit 1
fi

fill_drives "$WD/drives.toml"
