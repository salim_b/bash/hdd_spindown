#!/bin/bash
#
# This script looks for recent disk access, and if none is detected, puts `/dev/disk/by-label/${drive}` into standby mode (using smartmontools; i.a. suitable for OpenWrt).
#
# Parameters:
#
# - `$1`: directory to store (temporary) cache and log files; will default to the current working directory if unspecified
#
# Requirements:
#
# - Unix tools (OpenWrt package names):
#   - `lsblk`
#   - `smartmontools`
#
# - Bash scripts:
#   - [`cache_device_file_assignment.sh`](https://gitlab.com/salim_b/bash/hdd_spindown/-/raw/master/cache_device_file_assignment.sh)
#   - [`read_in_drives.sh`](https://gitlab.com/salim_b/bash/hdd_spindown/-/raw/master/read_in_drives.sh)
#
# - Config files:
#   - `drives.toml` with a line for each of the desired HDDs consisting of the partition label as key and the device type (as smartctl expects it) as value
#
# Notes:
#
# The first run of the script might take considerably longer because all of the specified drives are spun up by `lsblk` during partition label detection.

# determine directory from which the script is run (necessary for portability)
WD="${BASH_SOURCE%/*}"
if [[ ! -d "$WD" ]]; then WD="$PWD"; fi

# determine directory to temporarily store cache and log files
if [[ -z "$1" ]] ; then
  temp_dir="$WD"
else
  temp_dir="${1%/}"
fi

# read in `drives.toml` to `$drives`
source "${WD}/read_in_drives.sh"

# cache device file assignment to `$lsblk_file`
source "${WD}/cache_device_file_assignment.sh" "${temp_dir}"

lsblk_file="${temp_dir}/lsblk_name-label-model.txt"
log_file="${temp_dir}/hdd_access_log.md"
datetime=`date +"%Y-%m-%dT%T%z"`
LOG="false"

# create Markdown logfile header
if [[ ! -s ${log_file} && ${LOG} == "true" ]] ; then
    echo "| datetime | label | device | spindown_state | event |" >> ${log_file}
    echo "| :---: | :---- | :---- | :---: | :----------------- |" >> ${log_file}
fi

# process all drives specified in `drives.toml`
for label in ${!drives[*]} ; do

    device=`cat ${lsblk_file} | grep $label | grep -o "sd."`
    # model=`cat ${lsblk_file} | grep ^$device | grep -Eo "[[:space:]][[:alnum:]].+" | grep -Eo "[^[:space:]].*"`

    if [ ! -z ${device} ] ; then

        status_file="${temp_dir}/access_${device}.status"
        status_new=`cat /sys/block/${device}/stat`
        status_new=${status_new//[[:blank:]]/}

        if [ -s "${status_file}" ] ; then

            status_old=`cat "${status_file}"`
            status_old=${status_old//[[:blank:]]/}

            if [[ "${status_old}" == "${status_new}" ]] ; then

                # disable HDD controller's Advanced Power Management (APM) and spindown timeout (in order to avoid subsequent unintended spin-ups), and spin down HDD immediately
                spindown_cmd="smartctl -d ${drives[$label]} -n standby -s apm,off -s standby,off -s standby,now /dev/${device}"
                [ ${LOG} == "true" ] && echo "| ${datetime} | ${label} | /dev/${device} | NA | Drive shows no recent disk activity. Issuing spindown command. |" >> ${log_file}
                ${spindown_cmd} > /dev/null 2>&1

            else
                [ ${LOG} == "true" ] && echo "| ${datetime} | ${label} | /dev/${device} | NA | Drive shows recent disk activity. Doing nothing. |" >> ${log_file}
                echo ${status_new} > ${status_file}
            fi

        else
            [ ${LOG} == "true" ] && echo "| ${datetime} | ${label} | /dev/${device} | NA | Disk access status file not found or empty. Creating \`${status_file}\`. |" >> ${log_file}
            echo ${status_new} > "${status_file}"
        fi
    else
        echo "WARNING: No device file found for partition label \`${label}\`"
    fi
done

