#!/bin/bash
# This script deletes the cached device file assignment, access states and log files

if [[ -z "$1" ]] ; then
   path_prefix=""
else
   path_prefix="${1%/}/"
fi

if [ -s "${path_prefix}"lsblk_name-label-model.txt ] ; then rm "${path_prefix}"lsblk_name-label-model.txt ; fi
if [ -s "${path_prefix}"hdd_spindown_log.md ] ; then rm "${path_prefix}"hdd_spindown_log.md ; fi
if [ -s "${path_prefix}"hdd_access_log.md ] ; then rm "${path_prefix}"hdd_access_log.md ; fi
if ls "${path_prefix}"access_sd*.status 1> /dev/null 2>&1; then rm "${path_prefix}"access_sd*.status ; fi
