#!/bin/bash
# This script caches the current block device file assignment to the file `"$1"/lsblk_name-label-model.txt` in order to avoid unnecessary drive spin-ups.
#
# It's recommended to delete the file upon reboot. To do so, add the line `/opt/shell_scripts/hdd_spindown/clean_cache_and_logs.sh "/PATH/TO/TEMP/DIR/"` to the file `/etc/rc.local`.

# determine directory from which the script is run (necessary for portability)
WD="${BASH_SOURCE%/*}"
if [[ ! -d "$WD" ]]; then WD="$PWD"; fi

# determine paths to store cached files
if [[ -z "$1" ]] ; then
    path_prefix="$WD"
else
    path_prefix="${1%/}"
fi

lsblk_file="${path_prefix}/lsblk_name-label-model.txt"
last_device_file="${path_prefix}/last_device.txt"

# determine whether new storage devices were connected since last device file assignment caching
if [[ -s "${last_device_file}" ]] ; then
  last_device_before=$(cat "${last_device_file}")
else
  last_device_before="none"
fi

last_device_now=$(ls -1 /dev/sd? | tail -1)
echo "${last_device_now}" > "${last_device_file}"
last_device_changed="false"

if [ "${last_device_now}" != "${last_device_before}" ] ; then
  last_device_changed="true"
fi

# write device file assignment if necessary
if [[ ! -s "${lsblk_file}" || "${last_device_changed}" == "true" ]] ; then
  lsblk --output=NAME,LABEL,MODEL --tree=NAME --sort=NAME /dev/sd[![:digit:]] > "${lsblk_file}"
fi
