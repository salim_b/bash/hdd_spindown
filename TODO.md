# TODO

- use argbash for proper params

- also allow to define drives by
    - ID (as reported by `ls /dev/disk/by-id/`); simply use

        ```sh
        device=`readlink -nf /dev/disk/by-id/${device_id} | sed 's/[0-9]\+$//'`
        ```

    - UUID (as reported by `ls /dev/disk/by-uuid/`); simply use

        ```sh
        device=`readlink -nf /dev/disk/by-uuid/${device_id} | sed 's/[0-9]\+$//' | xargs basename`
        ```
